package fi.nope.minecraft.subspacecore;

public enum SubSpaceMessageType {
	BROADCAST, MESSAGE, TRANSPORT
}
