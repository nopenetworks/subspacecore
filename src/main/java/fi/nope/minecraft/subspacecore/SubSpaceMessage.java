package fi.nope.minecraft.subspacecore;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class SubSpaceMessage extends Event {
	private static final HandlerList HANDLERS = new HandlerList();
	private final SubSpaceMessageType messageType;
	private final String[] messageData;

	public SubSpaceMessage(SubSpaceMessageType messageType, String[] messageData) {
		this.messageType = messageType;
		this.messageData = messageData;
	}
	
	public SubSpaceMessageType getMessageType() {
        return this.messageType;
    }
	
	public String[] getMessageData() {
        return this.messageData;
    }
	
	public HandlerList getHandlers() {
		return HANDLERS;
	}

	public static HandlerList getHandlerList() {
		return HANDLERS;
	}

}
