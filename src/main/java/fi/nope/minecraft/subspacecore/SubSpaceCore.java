package fi.nope.minecraft.subspacecore;

import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

public class SubSpaceCore extends JavaPlugin implements CommandExecutor, Listener {
	FileConfiguration config = getConfig();

	@Override
	public void onEnable() {
		config.addDefault("debug", true);
	    config.options().copyDefaults(true);
	    saveConfig();
		
		getCommand("subspace").setExecutor(this);
		getServer().getPluginManager().registerEvents(this, this);
	}

	@Override
	public void onDisable() {

	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (args.length == 0) {
			sender.sendMessage("Help viesti tulee joskus :D");
			return true;
		} else if (args[0].equalsIgnoreCase("broadcast")) {
			SubSpaceMessage message = new SubSpaceMessage(SubSpaceMessageType.BROADCAST, Arrays.copyOfRange(args, 1, args.length));
			Bukkit.getPluginManager().callEvent(message);
			return true;
		} else if (args[0].equalsIgnoreCase("transport")) {
			SubSpaceMessage message = new SubSpaceMessage(SubSpaceMessageType.TRANSPORT, Arrays.copyOfRange(args, 1, args.length));
			Bukkit.getPluginManager().callEvent(message);
			return true;
		}
		return false;
	}

	@EventHandler
	public void onSubSpaceMessage(SubSpaceMessage message) {
		if (config.getBoolean("debug")) {
			Bukkit.broadcastMessage(ChatColor.GRAY + "[SubSpace Debug] " + ChatColor.RESET + message.getMessageType().toString() + " " + message.getMessageData().toString());
		}
	}
}
